﻿using UnityEngine;

public class Chest : MonoBehaviour
{
    [SerializeField] Item item;
    [SerializeField] Inventory inventory;

    private bool isInRange;
    private bool isEmpty;
    private Animator anim;

    private void OnValidate(){
        if(inventory == null)
        inventory = FindObjectOfType<Inventory>();

        anim = GetComponent<Animator>();
        
    }

    private void Update(){
        anim.SetBool("isEmpty", isEmpty);
        if(isInRange && Input.GetKeyDown(KeyCode.E)){
            if(!isEmpty){
                inventory.AddItem(item);
                isEmpty = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other){
        CheckCollision(other.gameObject, true);
    }

    private void OnTriggerExit2D(Collider2D other){
        CheckCollision(other.gameObject, false);
    }

    private void CheckCollision(GameObject gameObject, bool state){
        if (gameObject.CompareTag("Player")){
            isInRange = state;
        }
    }
}
